from django.urls import path, include
from . import views

app_name = 'main'
urlpatterns = [
    path('', views.DashboardView.as_view(), name='dashboard'),
    # login
    path('login/', views.LoginUser.as_view(), name='login-user'),
]

