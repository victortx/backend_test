from allauth.account.views import LoginView
from django.contrib.auth.models import User


class LoginUser(LoginView):

    def form_valid(self, form):
        success_url = self.get_success_url()
        data = self.request.POST
        user = User.objects.filter(username=data['login']).first()
        return form.login(self.request, redirect_url=success_url)